# Fuel consumption control restful application

To run locally with spring boot :

mvn spring-boot:run
service will be available at localhost:8080
project using H2 embedded database

## add new entry(single or multiple)
 
 ```
POST /consumption

body example

[{
                "price": 14.00,
                "volume": 13.00,
                "date": "2019-11-06",
                "driverId": "1234",
                "fuel": "G98"
},
{
                "price": 12.00,
                "volume": 13.00,
                "date": "2019-11-07",
                "driverId": "1234",
                "fuel": "G98"
},
{
                "price": 10.00,
                "volume": 50.00,
                "date": "2018-11-07",
                "driverId": "1234",
                "fuel": "G98"
}
]

```
Available fuel types: G95, G98, D

## Fuel consumption report

 ```
GET /consumption/report/total?driverId={driverId}
where driverId  is optional parameter. 
if driver id is not set then report will be generated for all entries

 ```

## Fuel consumption report for given month

 ```
GET /consumption/report/year/{YYYY}/month/{MM}?driverId={driverId}
where driverId  is optional parameter. 
if driver id is not set then report will be generated for all entries


 ```

## Fuel consumption  report for given fuel type

 ```
GET /consumption/report/fuel-type?driverId={driverId}
where driverId  is optional parameter. 
if driver id is not set then report will be generated for all entries

 ```