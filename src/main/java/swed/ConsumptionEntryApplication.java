package swed;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsumptionEntryApplication {

	public static void main(String... args) {
		SpringApplication.run(ConsumptionEntryApplication.class, args);
	}
}
