package swed.consumption;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ConsumptionEntryRepository extends JpaRepository<ConsumptionEntry, Long> {

    List<ConsumptionEntry> findAllByDriverId(String driverId);

    @Query("select e from ConsumptionEntry e where year(e.date) = ?1 and month(e.date) = ?2")
    List<ConsumptionEntry> getByYearAndMonth(int year, int month);

    @Query("select e from ConsumptionEntry e where year(e.date) = ?1 and month(e.date) = ?2 and e.driverId = ?3")
    List<ConsumptionEntry> getByYearAndMonthAndDriver(int year, int month, String driverId);

}
