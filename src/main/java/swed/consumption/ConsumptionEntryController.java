package swed.consumption;

import lombok.AllArgsConstructor;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@AllArgsConstructor
class ConsumptionEntryController {

    private final ConsumptionEntryRepository repository;
    private final ConsumptionEntryResourceAssembler assembler;
    private final ConsumptionService consumptionService;

    @GetMapping("/consumption")
    Resources<Resource<ConsumptionEntry>> all() {

        List<Resource<ConsumptionEntry>> employees = repository.findAll().stream()
                .map(assembler::toResource)
                .collect(Collectors.toList());

        return new Resources<>(employees,
                linkTo(methodOn(ConsumptionEntryController.class).all()).withSelfRel());
    }

    @PostMapping("/consumption")
    ResponseEntity<?> newEntries(@Valid @RequestBody List<ConsumptionEntry> newConsumptionEntry) {
        List<ConsumptionEntry> saved = repository.saveAll(newConsumptionEntry);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping("/consumption/{id}")
    Resource<ConsumptionEntry> one(@PathVariable Long id) {

        ConsumptionEntry consumptionEntry = repository.findById(id)
                .orElseThrow(() -> new ConsumptionEntryNotFoundException(id));

        return assembler.toResource(consumptionEntry);
    }

    @DeleteMapping("/consumption/{id}")
    ResponseEntity<?> deleteEmployee(@PathVariable Long id) {
        repository.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/consumption/report/total")
    ResponseEntity getConsumptionReport(@RequestParam(required = false) String driverId) {
        return ResponseEntity.ok(consumptionService.getAmountForDriver(driverId));
    }

    @GetMapping("/consumption/report/year/{year}/month/{month}")
    ResponseEntity getConsumptionReport(@RequestParam(required = false) String driverId, @PathVariable Integer year, @PathVariable Integer month) {
        if (driverId == null) {
            return ResponseEntity.ok(repository.getByYearAndMonth(year, month));
        } else {
            return ResponseEntity.ok(repository.getByYearAndMonthAndDriver(year, month, driverId));
        }

    }

    @GetMapping("/consumption/report/fuel-type")
    ResponseEntity getConsumptionReportByFuelType(@RequestParam(required = false) String driverId) {
        return ResponseEntity.ok(consumptionService.getFuelTypeStatistics(driverId));
    }

}
