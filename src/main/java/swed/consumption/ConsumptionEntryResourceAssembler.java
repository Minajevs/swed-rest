package swed.consumption;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
class ConsumptionEntryResourceAssembler implements ResourceAssembler<ConsumptionEntry, Resource<ConsumptionEntry>> {

	@Override
	public Resource<ConsumptionEntry> toResource(ConsumptionEntry consumptionEntry) {

		return new Resource<>(consumptionEntry,
			linkTo(methodOn(ConsumptionEntryController.class).one(consumptionEntry.getId())).withSelfRel(),
			linkTo(methodOn(ConsumptionEntryController.class).all()).withRel("employees"));
	}
}
