package swed.consumption;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class ConsumptionEntry {

    public enum Fuel {
        G98, G95, D
    }

    private @Id @GeneratedValue Long id;
    private @Positive @NotNull BigDecimal price;
    private @Positive BigDecimal volume;
    private LocalDate date;
    private @NotBlank String driverId;
    private Fuel fuel;

}
