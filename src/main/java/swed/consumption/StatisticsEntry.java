package swed.consumption;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class StatisticsEntry {

    private BigDecimal averagePrice;
    private BigDecimal totalPrice;
    private BigDecimal volume;
    private ConsumptionEntry.Fuel fuel;
    void addToTotal(BigDecimal amount) {
    	totalPrice = totalPrice.add(amount);
	}
	void addToVolume(BigDecimal amount) {
    	volume = volume.add(amount);
	}

}
