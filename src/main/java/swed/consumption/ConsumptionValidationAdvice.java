package swed.consumption;

import org.springframework.http.HttpStatus;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.ConstraintViolationException;
import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
class ConsumptionValidationAdvice {

	@ResponseBody
	@ExceptionHandler(ConsumptionEntryNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	String entryNotFoundHandler(ConsumptionEntryNotFoundException ex) {
		return ex.getMessage();
	}
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ConstraintViolationException.class)
    public Map<String, String> handleValidationExceptions(
            ConstraintViolationException ex) {
        Map<String, String> errors = new HashMap<>();
        mapErrors(ex, errors);
        return errors;
    }


    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(TransactionSystemException.class)
    public Map<String, String> handleValidationExceptions(
            TransactionSystemException ex) {
        Map<String, String> errors = new HashMap<>();
        mapErrors((ConstraintViolationException) ex.getOriginalException().getCause(), errors);
        return errors;
    }

    private void mapErrors(ConstraintViolationException ex, Map<String, String> errors) {
        ex.getConstraintViolations().forEach((error) -> {
            String fieldName = error.getPropertyPath().toString();
            String errorMessage = error.getMessage();
            errors.put(fieldName, errorMessage);
        });
    }
}
