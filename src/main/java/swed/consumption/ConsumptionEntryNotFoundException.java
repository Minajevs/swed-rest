package swed.consumption;

class ConsumptionEntryNotFoundException extends RuntimeException {

	ConsumptionEntryNotFoundException(Long id) {
		super("Could not find consumption " + id);
	}
}
