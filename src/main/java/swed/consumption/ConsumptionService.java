package swed.consumption;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.Month;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ConsumptionService {

    private final ConsumptionEntryRepository consumptionEntryRepository;

    Map<Integer, Map<Month, BigDecimal>> getAmountForDriver(String driverId) {

        List<ConsumptionEntry> consumptions;
        if (driverId == null) {
            consumptions = consumptionEntryRepository.findAll();
        } else {
            consumptions = consumptionEntryRepository.findAllByDriverId(driverId);
        }
        Map<Integer, List<ConsumptionEntry>> consumptionPerYearMap = getConsumptionPerYearMap(consumptions);

        Map<Integer, Map<Month, BigDecimal>> responseMap = new HashMap<>();

        consumptionPerYearMap.forEach((key, value) -> {

            Map<Month, List<BigDecimal>> monthMap = value.stream()
                    .collect(Collectors.groupingBy(entry -> entry.getDate().getMonth(),
                            Collectors.mapping(entry -> entry.getVolume().multiply(entry.getPrice()),
                                    Collectors.toList())));
            Map<Month, BigDecimal> monthToValue = monthMap.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey,
                    en -> en.getValue().stream().reduce(BigDecimal::add).get()));

            responseMap.put(key, monthToValue);

        });

        return responseMap;
    }

    Map<Integer, Map<Month, Map<ConsumptionEntry.Fuel, StatisticsEntry>>> getFuelTypeStatistics(String driverId) {

        List<ConsumptionEntry> consumptions;
        if (driverId == null) {
            consumptions = consumptionEntryRepository.findAll();
        } else {
            consumptions = consumptionEntryRepository.findAllByDriverId(driverId);
        }

        Map<Integer, List<ConsumptionEntry>> consumptionPerYearMap = getConsumptionPerYearMap(consumptions);

        Map<Integer, Map<Month, Map<ConsumptionEntry.Fuel, StatisticsEntry>>> responseMap = new HashMap<>();

        consumptionPerYearMap.forEach((year, consumptionThisYear) -> {

            Map<Month, List<ConsumptionEntry>> monthMap = consumptionThisYear.stream()
                    .collect(Collectors.groupingBy(entry -> entry.getDate().getMonth(),
                            Collectors.mapping(entry -> entry,
                                    Collectors.toList())));
            Map<Month, Map<ConsumptionEntry.Fuel, StatisticsEntry>> monthToGasType = new HashMap<>();
            monthMap.forEach((month, consumptionsThisMonth) -> {
                        Map<ConsumptionEntry.Fuel, List<StatisticsEntry>> fuelTypeToConsumptionStatistics = consumptionsThisMonth.stream()
                                .collect(Collectors.groupingBy(ConsumptionEntry::getFuel,
                                        Collectors.mapping(entry ->
                                                        StatisticsEntry.builder()
                                                                .fuel(entry.getFuel())
                                                                .volume(entry.getVolume())
                                                                .totalPrice(entry.getVolume().multiply(entry.getPrice()))
                                                                .build(),
                                                Collectors.toList())));

                        Map<ConsumptionEntry.Fuel, StatisticsEntry> fuelToStatistic = fuelTypeToConsumptionStatistics.entrySet()
                                .stream().collect(Collectors.toMap(Map.Entry::getKey, value ->
                                        value.getValue().stream().reduce((statisticsEntry, secondStatisticsEntry) -> {
                                            statisticsEntry.addToTotal(secondStatisticsEntry.getTotalPrice());
                                            statisticsEntry.addToVolume(secondStatisticsEntry.getVolume());
                                            return statisticsEntry;
                                        }).map(stats -> {
                                            stats.setAveragePrice(stats.getTotalPrice().divide(stats.getVolume(), 2));
                                            return stats;
                                        }).get())
                                );
                        monthToGasType.put(month, fuelToStatistic);
                    }
            );

            responseMap.put(year, monthToGasType);

        });

        return responseMap;
    }

    private Map<Integer, List<ConsumptionEntry>> getConsumptionPerYearMap(List<ConsumptionEntry> consumptions) {
        return consumptions.stream()
                .collect(Collectors.groupingBy(entry -> entry.getDate().getYear(),
                        Collectors.mapping(entry -> entry,
                                Collectors.toList())));
    }

}
