package swed.consumption;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static swed.consumption.ConsumptionEntry.Fuel.G95;
import static swed.consumption.ConsumptionEntry.Fuel.G98;

@ExtendWith(MockitoExtension.class)
class ConsumptionServiceTest {

    @InjectMocks
    ConsumptionService consumptionService;
    @Mock
    ConsumptionEntryRepository consumptionEntryRepository;

    @Test
    void getAmountForDriverNoEntriesWithDriver() {
        when(consumptionEntryRepository.findAllByDriverId("123")).thenReturn(new ArrayList<>());
        Map<Integer, Map<Month, BigDecimal>> map = consumptionService.getAmountForDriver("123");
        assertTrue(map.entrySet().isEmpty());

        verify(consumptionEntryRepository, never()).findAll();
    }
    @Test
    void getAmountForDriverNoEntriesNoDriver() {
        when(consumptionEntryRepository.findAll()).thenReturn(new ArrayList<>());
        Map<Integer, Map<Month, BigDecimal>> map = consumptionService.getAmountForDriver(null);
        assertTrue(map.entrySet().isEmpty());
        verify(consumptionEntryRepository, never()).findAllByDriverId(any());
    }

    @Test
    void getAmountForDriverDifferentYears() {
        ConsumptionEntry cons1 = new ConsumptionEntry();
        cons1.setDate(LocalDate.of(2019, 11, 20));
        cons1.setDriverId("123");
        cons1.setFuel(G98);
        cons1.setPrice(BigDecimal.valueOf(1.59));
        cons1.setVolume(BigDecimal.valueOf(15));

        ConsumptionEntry cons2 = new ConsumptionEntry();
        cons2.setDate(LocalDate.of(2018, 11, 10));
        cons2.setDriverId("123");
        cons2.setFuel(G98);
        cons2.setPrice(BigDecimal.valueOf(1.59));
        cons2.setVolume(BigDecimal.valueOf(50));

        when(consumptionEntryRepository.findAllByDriverId("123")).thenReturn(Arrays.asList(cons1, cons2));
        Map<Integer, Map<Month, BigDecimal>> map = consumptionService.getAmountForDriver("123");
        assertEquals(2,map.entrySet().size());
        assertTrue(map.containsKey(2019));
        assertTrue(map.containsKey(2018));
    }

    @Test
    void getAmountForDriverAdded() {

        ConsumptionEntry cons1 = new ConsumptionEntry();
        cons1.setDate(LocalDate.of(2019, 11, 20));
        cons1.setDriverId("123");
        cons1.setFuel(G98);
        cons1.setPrice(BigDecimal.valueOf(1.59));
        cons1.setVolume(BigDecimal.valueOf(15));

        ConsumptionEntry cons2 = new ConsumptionEntry();
        cons2.setDate(LocalDate.of(2019, 11, 10));
        cons2.setDriverId("123");
        cons2.setFuel(G98);
        cons2.setPrice(BigDecimal.valueOf(1.59));
        cons2.setVolume(BigDecimal.valueOf(50));

        ConsumptionEntry cons3 = new ConsumptionEntry();
        cons3.setDate(LocalDate.of(2019, 11, 9));
        cons3.setDriverId("123");
        cons3.setFuel(G98);
        cons3.setPrice(BigDecimal.valueOf(1.55));
        cons3.setVolume(BigDecimal.valueOf(40));

        when(consumptionEntryRepository.findAllByDriverId("123")).thenReturn(Arrays.asList(cons1, cons2, cons3));
        Map<Integer, Map<Month, BigDecimal>> map = consumptionService.getAmountForDriver("123");
        assertEquals(1,map.entrySet().size());
        assertTrue(map.containsKey(2019));
        assertEquals(BigDecimal.valueOf(165.35),map.get(2019).get(Month.NOVEMBER));

    }
    @Test
    void getAmountForDriverAddedDifferentMonths() {

        ConsumptionEntry cons1 = new ConsumptionEntry();
        cons1.setDate(LocalDate.of(2019, 11, 20));
        cons1.setDriverId("123");
        cons1.setFuel(G98);
        cons1.setPrice(BigDecimal.valueOf(1.59));
        cons1.setVolume(BigDecimal.valueOf(15));

        ConsumptionEntry cons2 = new ConsumptionEntry();
        cons2.setDate(LocalDate.of(2019, 2, 10));
        cons2.setDriverId("123");
        cons2.setFuel(G98);
        cons2.setPrice(BigDecimal.valueOf(1.59));
        cons2.setVolume(BigDecimal.valueOf(50));

        ConsumptionEntry cons3 = new ConsumptionEntry();
        cons3.setDate(LocalDate.of(2019, 11, 9));
        cons3.setDriverId("123");
        cons3.setFuel(G98);
        cons3.setPrice(BigDecimal.valueOf(1.55));
        cons3.setVolume(BigDecimal.valueOf(40));

        when(consumptionEntryRepository.findAllByDriverId("123")).thenReturn(Arrays.asList(cons1, cons2, cons3));
        Map<Integer, Map<Month, BigDecimal>> map = consumptionService.getAmountForDriver("123");
        assertEquals(1,map.entrySet().size());
        assertTrue(map.containsKey(2019));
        assertEquals(BigDecimal.valueOf(85.85),map.get(2019).get(Month.NOVEMBER));
        assertEquals(BigDecimal.valueOf(79.50).setScale(2,2),map.get(2019).get(Month.FEBRUARY));

    }

    @Test
    void getFuelTypeStatisticsNoEntries() {
        when(consumptionEntryRepository.findAllByDriverId("123")).thenReturn(new ArrayList<>());
        Map<Integer, Map<Month, Map<ConsumptionEntry.Fuel, StatisticsEntry>>> map = consumptionService.getFuelTypeStatistics("123");
        assertTrue(map.entrySet().isEmpty());
        verify(consumptionEntryRepository, never()).findAll();
    }

    @Test
    void getFuelTypeStatisticsNoEntriesNoDriver() {
        when(consumptionEntryRepository.findAll()).thenReturn(new ArrayList<>());
        Map<Integer, Map<Month, Map<ConsumptionEntry.Fuel, StatisticsEntry>>> map = consumptionService.getFuelTypeStatistics(null);
        assertTrue(map.entrySet().isEmpty());
        verify(consumptionEntryRepository, never()).findAllByDriverId(any());
    }

    @Test
    void getFuelTypeStatisticsDifferentYears() {
        ConsumptionEntry cons1 = new ConsumptionEntry();
        cons1.setDate(LocalDate.of(2019, 11, 20));
        cons1.setDriverId("123");
        cons1.setFuel(G98);
        cons1.setPrice(BigDecimal.valueOf(1.59));
        cons1.setVolume(BigDecimal.valueOf(15));

        ConsumptionEntry cons2 = new ConsumptionEntry();
        cons2.setDate(LocalDate.of(2018, 11, 10));
        cons2.setDriverId("123");
        cons2.setFuel(G98);
        cons2.setPrice(BigDecimal.valueOf(1.59));
        cons2.setVolume(BigDecimal.valueOf(50));

        when(consumptionEntryRepository.findAllByDriverId("123")).thenReturn(Arrays.asList(cons1, cons2));
        Map<Integer, Map<Month, Map<ConsumptionEntry.Fuel, StatisticsEntry>>> map = consumptionService.getFuelTypeStatistics("123");
        assertEquals(2,map.entrySet().size());
        assertTrue(map.containsKey(2019));
        assertTrue(map.containsKey(2018));
    }

    @Test
    void getFuelTypeStatisticsAdded() {

        ConsumptionEntry cons1 = new ConsumptionEntry();
        cons1.setDate(LocalDate.of(2019, 11, 20));
        cons1.setDriverId("123");
        cons1.setFuel(G98);
        cons1.setPrice(BigDecimal.valueOf(1.59));
        cons1.setVolume(BigDecimal.valueOf(15));

        ConsumptionEntry cons2 = new ConsumptionEntry();
        cons2.setDate(LocalDate.of(2019, 11, 10));
        cons2.setDriverId("123");
        cons2.setFuel(G98);
        cons2.setPrice(BigDecimal.valueOf(1.59));
        cons2.setVolume(BigDecimal.valueOf(50));

        ConsumptionEntry cons3 = new ConsumptionEntry();
        cons3.setDate(LocalDate.of(2019, 11, 9));
        cons3.setDriverId("123");
        cons3.setFuel(G98);
        cons3.setPrice(BigDecimal.valueOf(1.55));
        cons3.setVolume(BigDecimal.valueOf(40));

        when(consumptionEntryRepository.findAllByDriverId("123")).thenReturn(Arrays.asList(cons1, cons2, cons3));
        Map<Integer, Map<Month, Map<ConsumptionEntry.Fuel, StatisticsEntry>>> map = consumptionService.getFuelTypeStatistics("123");
        assertEquals(1,map.entrySet().size());
        assertTrue(map.containsKey(2019));
        assertEquals(BigDecimal.valueOf(165.35),map.get(2019).get(Month.NOVEMBER).get(G98).getTotalPrice());
        assertEquals(BigDecimal.valueOf(105),map.get(2019).get(Month.NOVEMBER).get(G98).getVolume());
        assertEquals(BigDecimal.valueOf(1.58),map.get(2019).get(Month.NOVEMBER).get(G98).getAveragePrice());

    }
    @Test
    void getFuelTypeStatisticsDifferentMonths() {

        ConsumptionEntry cons1 = new ConsumptionEntry();
        cons1.setDate(LocalDate.of(2019, 11, 20));
        cons1.setDriverId("123");
        cons1.setFuel(G98);
        cons1.setPrice(BigDecimal.valueOf(1.59));
        cons1.setVolume(BigDecimal.valueOf(15));

        ConsumptionEntry cons2 = new ConsumptionEntry();
        cons2.setDate(LocalDate.of(2019, 2, 10));
        cons2.setDriverId("123");
        cons2.setFuel(G98);
        cons2.setPrice(BigDecimal.valueOf(1.59));
        cons2.setVolume(BigDecimal.valueOf(50));

        ConsumptionEntry cons3 = new ConsumptionEntry();
        cons3.setDate(LocalDate.of(2019, 11, 9));
        cons3.setDriverId("123");
        cons3.setFuel(G95);
        cons3.setPrice(BigDecimal.valueOf(1.55));
        cons3.setVolume(BigDecimal.valueOf(40));

        when(consumptionEntryRepository.findAllByDriverId("123")).thenReturn(Arrays.asList(cons1, cons2, cons3));
        Map<Integer, Map<Month, Map<ConsumptionEntry.Fuel, StatisticsEntry>>> map = consumptionService.getFuelTypeStatistics("123");
        assertEquals(1,map.entrySet().size());
        assertTrue(map.containsKey(2019));
        assertEquals(BigDecimal.valueOf(23.85),map.get(2019).get(Month.NOVEMBER).get(G98).getTotalPrice());
        assertEquals(BigDecimal.valueOf(62.00).setScale(2,2),map.get(2019).get(Month.NOVEMBER).get(G95).getTotalPrice());
        assertEquals(BigDecimal.valueOf(79.50).setScale(2,2),map.get(2019).get(Month.FEBRUARY).get(G98).getTotalPrice());

    }

}